from budget_sync.test.mock_bug import MockBug
from budget_sync.config import Config
from budget_sync.budget_graph import (
    BudgetGraphLoopError, BudgetGraph, Node, BudgetGraphMoneyWithNoMilestone,
    BudgetGraphBaseError, BudgetGraphMoneyMismatchForBudgetExcludingSubtasks,
    BudgetGraphMoneyMismatchForBudgetIncludingSubtasks,
    BudgetGraphNegativeMoney, BudgetGraphMilestoneMismatch,
    BudgetGraphNegativePayeeMoney, BudgetGraphPayeesParseError,
    BudgetGraphPayeesMoneyMismatch, BudgetGraphUnknownMilestone,
    BudgetGraphIncorrectRootForMilestone,
    BudgetGraphUnknownStatus, BudgetGraphUnknownAssignee)
from budget_sync.money import Money
from budget_sync.util import BugStatus
from typing import List, Type
import unittest


class TestErrorFormatting(unittest.TestCase):
    def test_budget_graph_incorrect_root_for_milestone(self):
        self.assertEqual(str(BudgetGraphIncorrectRootForMilestone(
            2, "milestone 1", 1)),
            "Bug #2 is not the canonical root bug for assigned milestone "
            "'milestone 1' but has no parent bug set: the milestone's "
            "canonical root bug is #1")

    def test_budget_graph_loop_error(self):
        self.assertEqual(str(BudgetGraphLoopError([1, 2, 3, 4, 5])),
                         "Detected Loop in Budget Graph: #5 -> #1 "
                         "-> #2 -> #3 -> #4 -> #5")
        self.assertEqual(str(BudgetGraphLoopError([1])),
                         "Detected Loop in Budget Graph: #1 -> #1")

    def test_budget_graph_money_with_no_milestone(self):
        self.assertEqual(str(BudgetGraphMoneyWithNoMilestone(1, 5)),
                         "Bug assigned money but without any assigned "
                         "milestone: #1")

    def test_budget_graph_milestone_mismatch(self):
        self.assertEqual(str(BudgetGraphMilestoneMismatch(1, 5)),
                         "Bug's assigned milestone doesn't match the "
                         "milestone assigned to the root bug: descendant "
                         "bug #1, root bug #5")

    def test_budget_graph_unknown_milestone(self):
        self.assertEqual(str(BudgetGraphUnknownMilestone(
            123, "fake milestone")),
            "failed to parse cf_nlnet_milestone field of bug "
            "#123: unknown milestone: 'fake milestone'")

    def test_budget_graph_unknown_status(self):
        self.assertEqual(str(BudgetGraphUnknownStatus(
            123, "fake status")),
            "failed to parse status field of bug "
            "#123: unknown status: 'fake status'")

    def test_budget_graph_unknown_assignee(self):
        self.assertEqual(str(BudgetGraphUnknownAssignee(
            123, "unknown@example.com")),
            "Bug #123 is assigned to an unknown person:"
            " 'unknown@example.com'")

    def test_budget_graph_money_mismatch(self):
        self.assertEqual(str(
            BudgetGraphMoneyMismatchForBudgetExcludingSubtasks(
                1, 5, "123.4")),
            "Budget assigned to task excluding subtasks "
            "(cf_budget field) doesn't match calculated value:"
            " bug #1, calculated value 123.4")
        self.assertEqual(str(
            BudgetGraphMoneyMismatchForBudgetIncludingSubtasks(
                1, 5, "123.4")),
            "Budget assigned to task including subtasks "
            "(cf_total_budget field) doesn't match calculated value:"
            " bug #1, calculated value 123.4")

    def test_budget_graph_negative_money(self):
        self.assertEqual(str(BudgetGraphNegativeMoney(1, 5)),
                         "Budget assigned to task is less than zero: bug #1")

    def test_budget_graph_negative_payee_money(self):
        self.assertEqual(str(BudgetGraphNegativePayeeMoney(1, 5, "payee1")),
                         "Budget assigned to payee for task is less than "
                         "zero: bug #1, payee 'payee1'")

    def test_budget_graph_payees_parse_error(self):
        self.assertEqual(str(
            BudgetGraphPayeesParseError(1, "my fake parse error")),
            "Failed to parse cf_payees_list field of bug #1: "
            "my fake parse error")

    def test_budget_graph_payees_money_mismatch(self):
        self.assertEqual(str(
            BudgetGraphPayeesMoneyMismatch(1, 5, Money(123), Money(456))),
            "Total budget assigned to payees (cf_payees_list) doesn't match "
            "expected value: bug #1, calculated total 123, expected value 456")


EXAMPLE_BUG1 = MockBug(bug_id=1,
                       cf_budget_parent=None,
                       cf_budget="0",
                       cf_total_budget="0",
                       cf_nlnet_milestone=None,
                       cf_payees_list="",
                       summary="")
EXAMPLE_LOOP1_BUG1 = MockBug(bug_id=1,
                             cf_budget_parent=1,
                             cf_budget="0",
                             cf_total_budget="0",
                             cf_nlnet_milestone=None,
                             cf_payees_list="",
                             summary="")
EXAMPLE_LOOP2_BUG1 = MockBug(bug_id=1,
                             cf_budget_parent=2,
                             cf_budget="0",
                             cf_total_budget="0",
                             cf_nlnet_milestone=None,
                             cf_payees_list="",
                             summary="")
EXAMPLE_LOOP2_BUG2 = MockBug(bug_id=2,
                             cf_budget_parent=1,
                             cf_budget="0",
                             cf_total_budget="0",
                             cf_nlnet_milestone=None,
                             cf_payees_list="",
                             summary="")
EXAMPLE_PARENT_BUG1 = MockBug(bug_id=1,
                              cf_budget_parent=None,
                              cf_budget="10",
                              cf_total_budget="20",
                              cf_nlnet_milestone="milestone 1",
                              cf_payees_list="",
                              summary="")
EXAMPLE_CHILD_BUG2 = MockBug(bug_id=2,
                             cf_budget_parent=1,
                             cf_budget="10",
                             cf_total_budget="10",
                             cf_nlnet_milestone="milestone 1",
                             cf_payees_list="",
                             summary="")

EXAMPLE_CONFIG = Config.from_str(
    """
    bugzilla_url = "https://bugzilla.example.com/"
    [people."person1"]
    aliases = ["person1_alias1", "alias1"]
    full_name = "Person One"
    [people."person2"]
    email = "person2@example.com"
    aliases = ["person1_alias2", "alias2", "person 2"]
    full_name = "Person Two"
    [people."person3"]
    email = "user@example.com"
    full_name = "Person Three"
    [milestones]
    "milestone 1" = { canonical_bug_id = 1 }
    "milestone 2" = { canonical_bug_id = 2 }
    """)


class TestBudgetGraph(unittest.TestCase):
    maxDiff = None

    def assertErrorTypesMatches(self, errors: List[BudgetGraphBaseError], template: List[Type]):
        def wrap_type_list(type_list: List[Type]):
            class TypeWrapper:
                def __init__(self, t):
                    self.t = t

                def __repr__(self):
                    return self.t.__name__

                def __eq__(self, other):
                    return self.t == other.t
            return [TypeWrapper(i) for i in type_list]
        error_types = []
        for error in errors:
            error_types.append(type(error))
        self.assertEqual(wrap_type_list(error_types), wrap_type_list(template))

    def test_repr(self):
        bg = BudgetGraph([EXAMPLE_PARENT_BUG1, EXAMPLE_CHILD_BUG2],
                         EXAMPLE_CONFIG)
        self.assertEqual(
            repr(bg),
            "BudgetGraph{nodes=[Node(graph=..., id=#1, root=#1, parent=None, "
            "budget_excluding_subtasks=10, budget_including_subtasks=20, "
            "fixed_budget_excluding_subtasks=10, "
            "fixed_budget_including_subtasks=20, milestone_str='milestone "
            "1', is_in_nlnet_mou=False, "
            "milestone=Milestone(config=..., identifier='milestone 1', "
            "canonical_bug_id=1), immediate_children=[#2], payments=[], "
            "status=BugStatus.CONFIRMED, assignee=Person<'person3'>, "
            "resolved_payments={}, payment_summaries={}), Node(graph=..., "
            "id=#2, root=#1, parent=#1, budget_excluding_subtasks=10, "
            "budget_including_subtasks=10, "
            "fixed_budget_excluding_subtasks=10, "
            "fixed_budget_including_subtasks=10, milestone_str='milestone "
            "1', is_in_nlnet_mou=True, "
            "milestone=Milestone(config=..., identifier='milestone 1', "
            "canonical_bug_id=1), immediate_children=[], payments=[], "
            "status=BugStatus.CONFIRMED, assignee=Person<'person3'>, "
            "resolved_payments={}, payment_summaries={})], roots=[#1], "
            "assigned_nodes={Person(config=..., identifier='person1', "
            "full_name='Person One', "
            "aliases=OrderedSet(['person1_alias1', 'alias1']), email=None): "
            "[], Person(config=..., identifier='person2', "
            "full_name='Person Two', "
            "aliases=OrderedSet(['person1_alias2', 'alias2', 'person 2']), "
            "email='person2@example.com'): [], Person(config=..., "
            "identifier='person3', full_name='Person Three', "
            "aliases=OrderedSet(), email='user@example.com'): [#1, #2]}, "
            "assigned_nodes_for_milestones={Milestone(config=..., "
            "identifier='milestone 1', canonical_bug_id=1): [#1, #2], "
            "Milestone(config=..., identifier='milestone 2', "
            "canonical_bug_id=2): []}, "
            "milestone_payments={Milestone(config=..., identifier='milestone "
            "1', canonical_bug_id=1): [], Milestone(config=..., "
            "identifier='milestone 2', canonical_bug_id=2): []}, "
            "payments={Person(config=..., identifier='person1', "
            "full_name='Person One', "
            "aliases=OrderedSet(['person1_alias1', 'alias1']), email=None): "
            "{Milestone(config=..., identifier='milestone 1', "
            "canonical_bug_id=1): [], Milestone(config=..., "
            "identifier='milestone 2', canonical_bug_id=2): []}, "
            "Person(config=..., identifier='person2', "
            "full_name='Person Two', "
            "aliases=OrderedSet(['person1_alias2', 'alias2', 'person 2']), "
            "email='person2@example.com'): {Milestone(config=..., "
            "identifier='milestone 1', canonical_bug_id=1): [], "
            "Milestone(config=..., identifier='milestone 2', "
            "canonical_bug_id=2): []}, Person(config=..., "
            "identifier='person3', full_name='Person Three', "
            "aliases=OrderedSet(), email='user@example.com'): "
            "{Milestone(config=..., identifier='milestone 1', "
            "canonical_bug_id=1): [], Milestone(config=..., "
            "identifier='milestone 2', canonical_bug_id=2): []}}, "
            "milestone_people={Milestone(config=..., identifier='milestone "
            "1', canonical_bug_id=1): OrderedSet(), Milestone(config=..., "
            "identifier='milestone 2', canonical_bug_id=2): OrderedSet()}}")
        bg = BudgetGraph([MockBug(bug_id=1, status="blah",
                                  assigned_to="unknown@example.com")],
                         EXAMPLE_CONFIG)
        self.assertEqual(
            repr(bg),
            "BudgetGraph{nodes=[Node(graph=..., id=#1, root=#1, parent=None, "
            "budget_excluding_subtasks=0, budget_including_subtasks=0, "
            "fixed_budget_excluding_subtasks=0, "
            "fixed_budget_including_subtasks=0, milestone_str=None, "
            "is_in_nlnet_mou=False, "
            "milestone=None, immediate_children=[], payments=[], "
            "status=<unknown status: 'blah'>, assignee=<unknown assignee: "
            "'unknown@example.com'>, resolved_payments={}, "
            "payment_summaries={})], roots=[#1], assigned_nodes=<failed>, "
            "assigned_nodes_for_milestones={Milestone(config=..., "
            "identifier='milestone 1', canonical_bug_id=1): [], "
            "Milestone(config=..., identifier='milestone 2', "
            "canonical_bug_id=2): []}, "
            "milestone_payments={Milestone(config=..., "
            "identifier='milestone 1', canonical_bug_id=1): [], "
            "Milestone(config=..., identifier='milestone 2', "
            "canonical_bug_id=2): []}, payments={Person(config=..., "
            "identifier='person1', full_name='Person One', "
            "aliases=OrderedSet(['person1_alias1', 'alias1']), email=None): "
            "{Milestone(config=..., identifier='milestone 1', "
            "canonical_bug_id=1): [], Milestone(config=..., "
            "identifier='milestone 2', canonical_bug_id=2): []}, "
            "Person(config=..., identifier='person2', "
            "full_name='Person Two', "
            "aliases=OrderedSet(['person1_alias2', 'alias2', "
            "'person 2']), email='person2@example.com'): "
            "{Milestone(config=..., identifier='milestone 1', "
            "canonical_bug_id=1): [], Milestone(config=..., "
            "identifier='milestone 2', canonical_bug_id=2): []}, "
            "Person(config=..., identifier='person3', "
            "full_name='Person Three', aliases=OrderedSet(), "
            "email='user@example.com'): {Milestone(config=..., "
            "identifier='milestone 1', canonical_bug_id=1): [], "
            "Milestone(config=..., identifier='milestone 2', "
            "canonical_bug_id=2): []}}, "
            "milestone_people={Milestone(config=..., identifier='milestone "
            "1', canonical_bug_id=1): OrderedSet(), Milestone(config=..., "
            "identifier='milestone 2', canonical_bug_id=2): OrderedSet()}}")
        bg = BudgetGraph([MockBug(bug_id=1, status="blah",
                                  assigned_to="unknown@example.com",
                                  cf_payees_list="""\
person1 = {paid=2020-03-15,amount=5}
alias1 = {paid=2020-03-15,amount=10}
person2 = {submitted=2020-03-15,amount=15}
alias2 = {paid=2020-03-16,amount=23}
""")],
                         EXAMPLE_CONFIG)
        self.assertEqual(
            repr(bg),
            "BudgetGraph{nodes=[Node(graph=..., id=#1, root=#1, parent=None, "
            "budget_excluding_subtasks=0, budget_including_subtasks=0, "
            "fixed_budget_excluding_subtasks=0, "
            "fixed_budget_including_subtasks=0, milestone_str=None, "
            "is_in_nlnet_mou=False, "
            "milestone=None, immediate_children=[], "
            "payments=[Payment(node=#1, payee=Person<'person1'>, "
            "payee_key='person1', amount=5, state=Paid, paid=2020-03-15, "
            "submitted=None), Payment(node=#1, payee=Person<'person1'>, "
            "payee_key='alias1', amount=10, state=Paid, paid=2020-03-15, "
            "submitted=None), Payment(node=#1, payee=Person<'person2'>, "
            "payee_key='person2', amount=15, state=Submitted, paid=None, "
            "submitted=2020-03-15), Payment(node=#1, "
            "payee=Person<'person2'>, payee_key='alias2', amount=23, "
            "state=Paid, paid=2020-03-16, submitted=None)], status=<unknown "
            "status: 'blah'>, assignee=<unknown assignee: "
            "'unknown@example.com'>, resolved_payments={Person(config=..., "
            "identifier='person1', full_name='Person One', "
            "aliases=OrderedSet(['person1_alias1', 'alias1']), email=None): "
            "[Payment(node=#1, payee=Person<'person1'>, payee_key='person1', "
            "amount=5, state=Paid, paid=2020-03-15, submitted=None), "
            "Payment(node=#1, payee=Person<'person1'>, payee_key='alias1', "
            "amount=10, state=Paid, paid=2020-03-15, submitted=None)], "
            "Person(config=..., identifier='person2', "
            "full_name='Person Two', "
            "aliases=OrderedSet(['person1_alias2', 'alias2', 'person 2']), "
            "email='person2@example.com'): [Payment(node=#1, "
            "payee=Person<'person2'>, payee_key='person2', amount=15, "
            "state=Submitted, paid=None, submitted=2020-03-15), "
            "Payment(node=#1, payee=Person<'person2'>, payee_key='alias2', "
            "amount=23, state=Paid, paid=2020-03-16, submitted=None)]}, "
            "payment_summaries={Person(config=..., identifier='person1', "
            "full_name='Person One', "
            "aliases=OrderedSet(['person1_alias1', 'alias1']), email=None): "
            "PaymentSummary(total=15, total_paid=15, total_submitted=15, "
            "submitted_date=None, paid_date=2020-03-15, "
            "state=PaymentSummaryState.Paid, payments=(Payment(node=#1, "
            "payee=Person<'person1'>, payee_key='person1', amount=5, "
            "state=Paid, paid=2020-03-15, submitted=None), Payment(node=#1, "
            "payee=Person<'person1'>, payee_key='alias1', amount=10, "
            "state=Paid, paid=2020-03-15, submitted=None))), "
            "Person(config=..., identifier='person2', "
            "full_name='Person Two', "
            "aliases=OrderedSet(['person1_alias2', 'alias2', 'person 2']), "
            "email='person2@example.com'): PaymentSummary(total=38, "
            "total_paid=23, total_submitted=38, submitted_date=None, "
            "paid_date=None, state=PaymentSummaryState.Inconsistent, "
            "payments=(Payment(node=#1, payee=Person<'person2'>, "
            "payee_key='person2', amount=15, state=Submitted, paid=None, "
            "submitted=2020-03-15), Payment(node=#1, "
            "payee=Person<'person2'>, payee_key='alias2', amount=23, "
            "state=Paid, paid=2020-03-16, submitted=None)))})], roots=[#1], "
            "assigned_nodes=<failed>, "
            "assigned_nodes_for_milestones={Milestone(config=..., "
            "identifier='milestone 1', canonical_bug_id=1): [], "
            "Milestone(config=..., identifier='milestone 2', "
            "canonical_bug_id=2): []}, "
            "milestone_payments={Milestone(config=..., identifier='milestone "
            "1', canonical_bug_id=1): [], Milestone(config=..., "
            "identifier='milestone 2', canonical_bug_id=2): []}, "
            "payments={Person(config=..., identifier='person1', "
            "full_name='Person One', "
            "aliases=OrderedSet(['person1_alias1', 'alias1']), email=None): "
            "{Milestone(config=..., identifier='milestone 1', "
            "canonical_bug_id=1): [], Milestone(config=..., "
            "identifier='milestone 2', canonical_bug_id=2): []}, "
            "Person(config=..., identifier='person2', "
            "full_name='Person Two', "
            "aliases=OrderedSet(['person1_alias2', 'alias2', 'person 2']), "
            "email='person2@example.com'): {Milestone(config=..., "
            "identifier='milestone 1', canonical_bug_id=1): [], "
            "Milestone(config=..., identifier='milestone 2', "
            "canonical_bug_id=2): []}, Person(config=..., "
            "identifier='person3', full_name='Person Three', "
            "aliases=OrderedSet(), email='user@example.com'): "
            "{Milestone(config=..., identifier='milestone 1', "
            "canonical_bug_id=1): [], Milestone(config=..., "
            "identifier='milestone 2', canonical_bug_id=2): []}}, "
            "milestone_people={Milestone(config=..., identifier='milestone "
            "1', canonical_bug_id=1): OrderedSet(), Milestone(config=..., "
            "identifier='milestone 2', canonical_bug_id=2): OrderedSet()}}")

    def test_empty(self):
        bg = BudgetGraph([], EXAMPLE_CONFIG)
        self.assertEqual(len(bg.nodes), 0)
        self.assertEqual(len(bg.roots), 0)
        self.assertIs(bg.config, EXAMPLE_CONFIG)

    def test_single(self):
        bg = BudgetGraph([EXAMPLE_BUG1], EXAMPLE_CONFIG)
        self.assertEqual(len(bg.nodes), 1)
        node: Node = bg.nodes[1]
        self.assertEqual(bg.roots, {node})
        self.assertIsInstance(node, Node)
        self.assertIs(node.graph, bg)
        self.assertIs(node.bug, EXAMPLE_BUG1)
        self.assertIs(node.root, node)
        self.assertIsNone(node.parent_id)
        self.assertEqual(node.immediate_children, set())
        self.assertEqual(node.bug_url,
                         "https://bugzilla.example.com/show_bug.cgi?id=1")
        self.assertEqual(node.budget_excluding_subtasks, Money(cents=0))
        self.assertEqual(node.budget_including_subtasks, Money(cents=0))
        self.assertIsNone(node.milestone)
        self.assertEqual(node.payments, {})

    def test_loop1(self):
        with self.assertRaises(BudgetGraphLoopError) as cm:
            BudgetGraph([EXAMPLE_LOOP1_BUG1], EXAMPLE_CONFIG).roots
        self.assertEqual(cm.exception.bug_ids, [1])

    def test_loop2(self):
        with self.assertRaises(BudgetGraphLoopError) as cm:
            BudgetGraph([EXAMPLE_LOOP2_BUG1, EXAMPLE_LOOP2_BUG2],
                        EXAMPLE_CONFIG).roots
        self.assertEqual(cm.exception.bug_ids, [2, 1])

    def test_parent_child(self):
        bg = BudgetGraph([EXAMPLE_PARENT_BUG1, EXAMPLE_CHILD_BUG2],
                         EXAMPLE_CONFIG)
        self.assertEqual(len(bg.nodes), 2)
        node1: Node = bg.nodes[1]
        node2: Node = bg.nodes[2]
        self.assertEqual(bg.roots, {node1})
        self.assertEqual(node1, node1)
        self.assertEqual(node2, node2)
        self.assertNotEqual(node1, node2)
        self.assertNotEqual(node2, node1)
        self.assertIsInstance(node1, Node)
        self.assertIs(node1.graph, bg)
        self.assertIs(node1.bug, EXAMPLE_PARENT_BUG1)
        self.assertIsNone(node1.parent_id)
        self.assertEqual(node1.root, node1)
        self.assertEqual(node1.immediate_children, {node2})
        self.assertEqual(node1.budget_excluding_subtasks, Money(cents=1000))
        self.assertEqual(node1.budget_including_subtasks, Money(cents=2000))
        self.assertEqual(node1.milestone_str, "milestone 1")
        self.assertEqual(node1.bug_url,
                         "https://bugzilla.example.com/show_bug.cgi?id=1")
        self.assertEqual(list(node1.children()), [node2])
        self.assertEqual(list(node1.children_breadth_first()), [node2])
        self.assertEqual(node1.payments, {})
        self.assertIsInstance(node2, Node)
        self.assertIs(node2.graph, bg)
        self.assertIs(node2.bug, EXAMPLE_CHILD_BUG2)
        self.assertEqual(node2.parent_id, 1)
        self.assertEqual(node2.root, node1)
        self.assertEqual(node2.immediate_children, set())
        self.assertEqual(node2.budget_excluding_subtasks, Money(cents=1000))
        self.assertEqual(node2.budget_including_subtasks, Money(cents=1000))
        self.assertEqual(node2.milestone_str, "milestone 1")
        self.assertEqual(node2.payments, {})
        self.assertEqual(node2.bug_url,
                         "https://bugzilla.example.com/show_bug.cgi?id=2")

    def test_children(self):
        bg = BudgetGraph([
            MockBug(bug_id=1,
                    cf_budget_parent=None,
                    cf_budget="0",
                    cf_total_budget="0",
                    cf_nlnet_milestone=None,
                    cf_payees_list="",
                    summary=""),
            MockBug(bug_id=2,
                    cf_budget_parent=1,
                    cf_budget="0",
                    cf_total_budget="0",
                    cf_nlnet_milestone=None,
                    cf_payees_list="",
                    summary=""),
            MockBug(bug_id=3,
                    cf_budget_parent=1,
                    cf_budget="0",
                    cf_total_budget="0",
                    cf_nlnet_milestone=None,
                    cf_payees_list="",
                    summary=""),
            MockBug(bug_id=4,
                    cf_budget_parent=1,
                    cf_budget="0",
                    cf_total_budget="0",
                    cf_nlnet_milestone=None,
                    cf_payees_list="",
                    summary=""),
            MockBug(bug_id=5,
                    cf_budget_parent=3,
                    cf_budget="0",
                    cf_total_budget="0",
                    cf_nlnet_milestone=None,
                    cf_payees_list="",
                    summary=""),
            MockBug(bug_id=6,
                    cf_budget_parent=3,
                    cf_budget="0",
                    cf_total_budget="0",
                    cf_nlnet_milestone=None,
                    cf_payees_list="",
                    summary=""),
            MockBug(bug_id=7,
                    cf_budget_parent=5,
                    cf_budget="0",
                    cf_total_budget="0",
                    cf_nlnet_milestone=None,
                    cf_payees_list="",
                    summary=""),
        ], EXAMPLE_CONFIG)
        self.assertEqual(len(bg.nodes), 7)
        node1: Node = bg.nodes[1]
        node2: Node = bg.nodes[2]
        node3: Node = bg.nodes[3]
        node4: Node = bg.nodes[4]
        node5: Node = bg.nodes[5]
        node6: Node = bg.nodes[6]
        node7: Node = bg.nodes[7]
        self.assertEqual(bg.roots, {node1})
        self.assertEqual(list(node1.children()),
                         [node2, node3, node5, node7, node6, node4])
        self.assertEqual(list(node1.children_breadth_first()),
                         [node2, node3, node4, node5, node6, node7])

    def test_money_with_no_milestone(self):
        bg = BudgetGraph([
            MockBug(bug_id=1,
                    cf_budget_parent=None,
                    cf_budget="0",
                    cf_total_budget="10",
                    cf_nlnet_milestone=None,
                    cf_payees_list="",
                    summary=""),
        ], EXAMPLE_CONFIG)
        errors = bg.get_errors()
        self.assertErrorTypesMatches(errors, [
            BudgetGraphMoneyWithNoMilestone,
            BudgetGraphMoneyMismatchForBudgetExcludingSubtasks])
        self.assertEqual(errors[0].bug_id, 1)
        self.assertEqual(errors[0].root_bug_id, 1)
        bg = BudgetGraph([
            MockBug(bug_id=1,
                    cf_budget_parent=None,
                    cf_budget="10",
                    cf_total_budget="0",
                    cf_nlnet_milestone=None,
                    cf_payees_list="",
                    summary=""),
        ], EXAMPLE_CONFIG)
        errors = bg.get_errors()
        self.assertErrorTypesMatches(errors, [
            BudgetGraphMoneyWithNoMilestone,
            BudgetGraphMoneyMismatchForBudgetIncludingSubtasks])
        self.assertEqual(errors[0].bug_id, 1)
        self.assertEqual(errors[0].root_bug_id, 1)
        bg = BudgetGraph([
            MockBug(bug_id=1,
                    cf_budget_parent=None,
                    cf_budget="10",
                    cf_total_budget="10",
                    cf_nlnet_milestone=None,
                    cf_payees_list="",
                    summary=""),
        ], EXAMPLE_CONFIG)
        errors = bg.get_errors()
        self.assertErrorTypesMatches(errors, [BudgetGraphMoneyWithNoMilestone])
        self.assertEqual(errors[0].bug_id, 1)
        self.assertEqual(errors[0].root_bug_id, 1)

    def test_money_mismatch(self):
        def helper(budget, total_budget, payees_list, child_budget,
                   expected_errors, expected_fixed_error_types=None):
            if expected_fixed_error_types is None:
                expected_fixed_error_types = []
            bg = BudgetGraph([
                MockBug(bug_id=1,
                        cf_budget_parent=None,
                        cf_budget=budget,
                        cf_total_budget=total_budget,
                        cf_nlnet_milestone="milestone 1",
                        cf_payees_list=payees_list,
                        summary=""),
                MockBug(bug_id=2,
                        cf_budget_parent=1,
                        cf_budget=child_budget,
                        cf_total_budget=child_budget,
                        cf_nlnet_milestone="milestone 1",
                        summary=""),
            ], EXAMPLE_CONFIG)
            node1: Node = bg.nodes[1]
            errors = bg.get_errors()
            self.assertErrorTypesMatches(errors,
                                         [type(i) for i in expected_errors])
            self.assertEqual([str(i) for i in errors],
                             [str(i) for i in expected_errors])
            bg = BudgetGraph([
                MockBug(bug_id=1,
                        cf_budget_parent=None,
                        cf_budget=str(node1.fixed_budget_excluding_subtasks),
                        cf_total_budget=str(
                            node1.fixed_budget_including_subtasks),
                        cf_nlnet_milestone="milestone 1",
                        cf_payees_list=payees_list,
                        summary=""),
                MockBug(bug_id=2,
                        cf_budget_parent=1,
                        cf_budget=child_budget,
                        cf_total_budget=child_budget,
                        cf_nlnet_milestone="milestone 1",
                        cf_payees_list="",
                        summary=""),
            ], EXAMPLE_CONFIG)
            errors = bg.get_errors()
            self.assertErrorTypesMatches(errors,
                                         expected_fixed_error_types)
        helper(budget="0",
               total_budget="0",
               payees_list="",
               child_budget="0",
               expected_errors=[])
        helper(budget="0",
               total_budget="0",
               payees_list="",
               child_budget="5",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetIncludingSubtasks(
                       1, 1, Money(5)),
               ])
        helper(budget="0",
               total_budget="0",
               payees_list="person1=1",
               child_budget="0",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetExcludingSubtasks(
                       1, 1, Money(1)),
                   BudgetGraphMoneyMismatchForBudgetIncludingSubtasks(
                       1, 1, Money(1)),
               ])
        helper(budget="0",
               total_budget="0",
               payees_list="person1=1",
               child_budget="5",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetExcludingSubtasks(
                       1, 1, Money(1)),
                   BudgetGraphMoneyMismatchForBudgetIncludingSubtasks(
                       1, 1, Money(6)),
               ])
        helper(budget="0",
               total_budget="0",
               payees_list="person1=10",
               child_budget="0",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetExcludingSubtasks(
                       1, 1, Money(10)),
                   BudgetGraphMoneyMismatchForBudgetIncludingSubtasks(
                       1, 1, Money(10)),
               ])
        helper(budget="0",
               total_budget="0",
               payees_list="person1=10",
               child_budget="5",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetExcludingSubtasks(
                       1, 1, Money(10)),
                   BudgetGraphMoneyMismatchForBudgetIncludingSubtasks(
                       1, 1, Money(15)),
               ])
        helper(budget="0",
               total_budget="100",
               payees_list="",
               child_budget="0",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetExcludingSubtasks(
                       1, 1, Money(100)),
               ])
        helper(budget="0",
               total_budget="100",
               payees_list="",
               child_budget="5",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetExcludingSubtasks(
                       1, 1, Money(95)),
               ])
        helper(budget="0",
               total_budget="100",
               payees_list="person1=1",
               child_budget="0",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetExcludingSubtasks(
                       1, 1, Money(100)),
                   BudgetGraphPayeesMoneyMismatch(1, 1, Money(1), Money(100)),
               ],
               expected_fixed_error_types=[BudgetGraphPayeesMoneyMismatch])
        helper(budget="0",
               total_budget="100",
               payees_list="person1=1",
               child_budget="5",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetExcludingSubtasks(
                       1, 1, Money(95)),
                   BudgetGraphPayeesMoneyMismatch(1, 1, Money(1), Money(95)),
               ],
               expected_fixed_error_types=[BudgetGraphPayeesMoneyMismatch])
        helper(budget="0",
               total_budget="100",
               payees_list="person1=10",
               child_budget="0",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetExcludingSubtasks(
                       1, 1, Money(100)),
                   BudgetGraphPayeesMoneyMismatch(1, 1, Money(10), Money(100)),
               ],
               expected_fixed_error_types=[BudgetGraphPayeesMoneyMismatch])
        helper(budget="0",
               total_budget="100",
               payees_list="person1=10",
               child_budget="5",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetExcludingSubtasks(
                       1, 1, Money(95)),
                   BudgetGraphPayeesMoneyMismatch(1, 1, Money(10), Money(95)),
               ],
               expected_fixed_error_types=[BudgetGraphPayeesMoneyMismatch])
        helper(budget="0",
               total_budget="5",
               payees_list="",
               child_budget="5",
               expected_errors=[])
        helper(budget="0",
               total_budget="5",
               payees_list="person1=1",
               child_budget="5",
               expected_errors=[
                   BudgetGraphPayeesMoneyMismatch(1, 1, Money(1), Money(0)),
               ],
               expected_fixed_error_types=[BudgetGraphPayeesMoneyMismatch])
        helper(budget="0",
               total_budget="5",
               payees_list="person1=10",
               child_budget="5",
               expected_errors=[
                   BudgetGraphPayeesMoneyMismatch(1, 1, Money(10), Money(0)),
               ],
               expected_fixed_error_types=[BudgetGraphPayeesMoneyMismatch])
        helper(budget="10",
               total_budget="0",
               payees_list="",
               child_budget="0",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetIncludingSubtasks(
                       1, 1, Money(10)),
               ])
        helper(budget="10",
               total_budget="0",
               payees_list="",
               child_budget="5",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetIncludingSubtasks(
                       1, 1, Money(15)),
               ])
        helper(budget="10",
               total_budget="0",
               payees_list="person1=1",
               child_budget="0",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetIncludingSubtasks(
                       1, 1, Money(10)),
                   BudgetGraphPayeesMoneyMismatch(1, 1, Money(1), Money(10)),
               ],
               expected_fixed_error_types=[BudgetGraphPayeesMoneyMismatch])
        helper(budget="10",
               total_budget="0",
               payees_list="person1=1",
               child_budget="5",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetIncludingSubtasks(
                       1, 1, Money(15)),
                   BudgetGraphPayeesMoneyMismatch(1, 1, Money(1), Money(10)),
               ],
               expected_fixed_error_types=[BudgetGraphPayeesMoneyMismatch])
        helper(budget="10",
               total_budget="0",
               payees_list="person1=10",
               child_budget="0",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetIncludingSubtasks(
                       1, 1, Money(10)),
               ])
        helper(budget="10",
               total_budget="0",
               payees_list="person1=10",
               child_budget="5",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetIncludingSubtasks(
                       1, 1, Money(15)),
               ])
        helper(budget="10",
               total_budget="10",
               payees_list="",
               child_budget="0",
               expected_errors=[])
        helper(budget="10",
               total_budget="10",
               payees_list="person1=1",
               child_budget="0",
               expected_errors=[
                   BudgetGraphPayeesMoneyMismatch(1, 1, Money(1), Money(10)),
               ],
               expected_fixed_error_types=[BudgetGraphPayeesMoneyMismatch])
        helper(budget="10",
               total_budget="10",
               payees_list="person1=10",
               child_budget="0",
               expected_errors=[])
        helper(budget="10",
               total_budget="100",
               payees_list="",
               child_budget="0",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetExcludingSubtasks(
                       1, 1, Money(100)),
               ])
        helper(budget="10",
               total_budget="100",
               payees_list="",
               child_budget="5",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetExcludingSubtasks(
                       1, 1, Money(95)),
               ])
        helper(budget="10",
               total_budget="100",
               payees_list="person1=1",
               child_budget="0",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetIncludingSubtasks(
                       1, 1, Money(10)),
                   BudgetGraphPayeesMoneyMismatch(1, 1, Money(1), Money(10)),
               ],
               expected_fixed_error_types=[BudgetGraphPayeesMoneyMismatch])
        helper(budget="10",
               total_budget="100",
               payees_list="person1=1",
               child_budget="5",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetIncludingSubtasks(
                       1, 1, Money(15)),
                   BudgetGraphPayeesMoneyMismatch(1, 1, Money(1), Money(10)),
               ],
               expected_fixed_error_types=[BudgetGraphPayeesMoneyMismatch])
        helper(budget="10",
               total_budget="100",
               payees_list="person1=10",
               child_budget="0",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetIncludingSubtasks(
                       1, 1, Money(10)),
               ])
        helper(budget="10",
               total_budget="100",
               payees_list="person1=10",
               child_budget="5",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetIncludingSubtasks(
                       1, 1, Money(15)),
               ])
        helper(budget="10",
               total_budget="15",
               payees_list="",
               child_budget="5",
               expected_errors=[])
        helper(budget="10",
               total_budget="15",
               payees_list="person1=1",
               child_budget="5",
               expected_errors=[
                   BudgetGraphPayeesMoneyMismatch(1, 1, Money(1), Money(10)),
               ],
               expected_fixed_error_types=[BudgetGraphPayeesMoneyMismatch])
        helper(budget="10",
               total_budget="15",
               payees_list="person1=10",
               child_budget="5",
               expected_errors=[])

        helper(budget="1",
               total_budget="15",
               payees_list="person1=10",
               child_budget="5",
               expected_errors=[
                   BudgetGraphMoneyMismatchForBudgetExcludingSubtasks(
                       1, 1, Money("10"))
               ])

    def test_negative_money(self):
        bg = BudgetGraph([
            MockBug(bug_id=1,
                    cf_budget_parent=None,
                    cf_budget="0",
                    cf_total_budget="-10",
                    cf_nlnet_milestone="milestone 1",
                    cf_payees_list="",
                    summary=""),
        ], EXAMPLE_CONFIG)
        errors = bg.get_errors()
        self.assertErrorTypesMatches(errors, [
            BudgetGraphNegativeMoney,
            BudgetGraphMoneyMismatchForBudgetIncludingSubtasks])
        self.assertEqual(errors[0].bug_id, 1)
        self.assertEqual(errors[0].root_bug_id, 1)
        self.assertEqual(errors[1].bug_id, 1)
        self.assertEqual(errors[1].root_bug_id, 1)
        self.assertEqual(errors[1].expected_budget_including_subtasks, 0)
        bg = BudgetGraph([
            MockBug(bug_id=1,
                    cf_budget_parent=None,
                    cf_budget="-10",
                    cf_total_budget="0",
                    cf_nlnet_milestone="milestone 1",
                    cf_payees_list="",
                    summary=""),
        ], EXAMPLE_CONFIG)
        errors = bg.get_errors()
        self.assertErrorTypesMatches(errors, [
            BudgetGraphNegativeMoney,
            BudgetGraphMoneyMismatchForBudgetIncludingSubtasks])
        self.assertEqual(errors[0].bug_id, 1)
        self.assertEqual(errors[0].root_bug_id, 1)
        self.assertEqual(errors[1].bug_id, 1)
        self.assertEqual(errors[1].root_bug_id, 1)
        self.assertEqual(errors[1].expected_budget_including_subtasks, -10)
        bg = BudgetGraph([
            MockBug(bug_id=1,
                    cf_budget_parent=None,
                    cf_budget="-10",
                    cf_total_budget="-10",
                    cf_nlnet_milestone="milestone 1",
                    cf_payees_list="",
                    summary=""),
        ], EXAMPLE_CONFIG)
        errors = bg.get_errors()
        self.assertErrorTypesMatches(errors,
                                     [BudgetGraphNegativeMoney])
        self.assertEqual(errors[0].bug_id, 1)
        self.assertEqual(errors[0].root_bug_id, 1)

    def test_payees_parse(self):
        def check(cf_payees_list, error_types, expected_payments):
            bg = BudgetGraph([MockBug(bug_id=1,
                                      cf_budget_parent=None,
                                      cf_budget="0",
                                      cf_total_budget="0",
                                      cf_nlnet_milestone="milestone 1",
                                      cf_payees_list=cf_payees_list,
                                      summary=""),
                              ], EXAMPLE_CONFIG)
            self.assertErrorTypesMatches(bg.get_errors(), error_types)
            self.assertEqual(len(bg.nodes), 1)
            node: Node = bg.nodes[1]
            self.assertEqual([str(i) for i in node.payments.values()],
                             expected_payments)

        check(
            """
            person1 = 123
            """,
            [BudgetGraphMoneyMismatchForBudgetExcludingSubtasks,
             BudgetGraphMoneyMismatchForBudgetIncludingSubtasks],
            ["Payment(node=#1, payee=Person<'person1'>, "
             "payee_key='person1', amount=123, "
             "state=NotYetSubmitted, paid=None, submitted=None)"])
        check(
            """
            abc = "123"
            """,
            [BudgetGraphPayeesParseError,
             BudgetGraphMoneyMismatchForBudgetExcludingSubtasks,
             BudgetGraphMoneyMismatchForBudgetIncludingSubtasks],
            ["Payment(node=#1, payee=<unknown person>, payee_key='abc', "
             "amount=123, state=NotYetSubmitted, paid=None, "
             "submitted=None)"])
        check(
            """
            person1 = "123.45"
            """,
            [BudgetGraphMoneyMismatchForBudgetExcludingSubtasks,
             BudgetGraphMoneyMismatchForBudgetIncludingSubtasks],
            ["Payment(node=#1, payee=Person<'person1'>, "
             "payee_key='person1', amount=123.45, "
             "state=NotYetSubmitted, paid=None, submitted=None)"])
        check(
            """
            person1 = "123.45"
            "person 2" = "21.35"
            """,
            [BudgetGraphMoneyMismatchForBudgetExcludingSubtasks,
             BudgetGraphMoneyMismatchForBudgetIncludingSubtasks],
            ["Payment(node=#1, payee=Person<'person1'>, payee_key='person1', "
             'amount=123.45, state=NotYetSubmitted, paid=None, '
             'submitted=None)',
             "Payment(node=#1, payee=Person<'person2'>, payee_key='person 2', "
             'amount=21.35, state=NotYetSubmitted, paid=None, '
             'submitted=None)'])
        check(
            """
            person1 = "123.45"
            "d e f" = "21.35"
            """,
            [BudgetGraphPayeesParseError,
             BudgetGraphMoneyMismatchForBudgetExcludingSubtasks,
             BudgetGraphMoneyMismatchForBudgetIncludingSubtasks],
            ["Payment(node=#1, payee=Person<'person1'>, payee_key='person1', "
             'amount=123.45, state=NotYetSubmitted, paid=None, '
             'submitted=None)',
             "Payment(node=#1, payee=<unknown person>, payee_key='d e f', "
             'amount=21.35, state=NotYetSubmitted, paid=None, '
             'submitted=None)'])
        check(
            """
            abc = "123.45"
            # my comments
            "AAA" = "-21.35"
            """,
            [BudgetGraphPayeesParseError,
             BudgetGraphNegativePayeeMoney,
             BudgetGraphPayeesParseError,
             BudgetGraphMoneyMismatchForBudgetExcludingSubtasks,
             BudgetGraphMoneyMismatchForBudgetIncludingSubtasks],
            ["Payment(node=#1, payee=<unknown person>, payee_key='abc', "
             'amount=123.45, state=NotYetSubmitted, paid=None, '
             'submitted=None)',
             "Payment(node=#1, payee=<unknown person>, payee_key='AAA', "
             'amount=-21.35, state=NotYetSubmitted, paid=None, '
             'submitted=None)'])
        check(
            """
            "not-an-email@example.com" = "-2345"
            """,
            [BudgetGraphNegativePayeeMoney,
             BudgetGraphPayeesParseError,
             BudgetGraphMoneyMismatchForBudgetExcludingSubtasks,
             BudgetGraphMoneyMismatchForBudgetIncludingSubtasks],
            ['Payment(node=#1, payee=<unknown person>, '
             "payee_key='not-an-email@example.com', amount=-2345, "
             "state=NotYetSubmitted, paid=None, submitted=None)"])
        check(
            """
            person1 = { amount = 123 }
            """,
            [BudgetGraphMoneyMismatchForBudgetExcludingSubtasks,
             BudgetGraphMoneyMismatchForBudgetIncludingSubtasks],
            ["Payment(node=#1, payee=Person<'person1'>, payee_key='person1', "
             "amount=123, state=NotYetSubmitted, paid=None, submitted=None)"])
        check(
            """
            person1 = { amount = 123, submitted = 2020-05-01 }
            """,
            [BudgetGraphMoneyMismatchForBudgetExcludingSubtasks,
             BudgetGraphMoneyMismatchForBudgetIncludingSubtasks],
            ["Payment(node=#1, payee=Person<'person1'>, payee_key='person1', "
             + "amount=123, state=Submitted, paid=None, "
             + "submitted=2020-05-01)"])
        check(
            """
            person1 = { amount = 123, submitted = 2020-05-01T00:00:00 }
            """,
            [BudgetGraphMoneyMismatchForBudgetExcludingSubtasks,
             BudgetGraphMoneyMismatchForBudgetIncludingSubtasks],
            ["Payment(node=#1, payee=Person<'person1'>, payee_key='person1', "
             + "amount=123, state=Submitted, paid=None, "
             + "submitted=2020-05-01 00:00:00)"])
        check(
            """
            person1 = { amount = 123, submitted = 2020-05-01T00:00:00Z }
            """,
            [BudgetGraphMoneyMismatchForBudgetExcludingSubtasks,
             BudgetGraphMoneyMismatchForBudgetIncludingSubtasks],
            ["Payment(node=#1, payee=Person<'person1'>, payee_key='person1', "
             + "amount=123, state=Submitted, paid=None, "
             + "submitted=2020-05-01 00:00:00+00:00)"])
        check(
            """
            person1 = { amount = 123, submitted = 2020-05-01T00:00:00-07:23 }
            """,
            [BudgetGraphMoneyMismatchForBudgetExcludingSubtasks,
             BudgetGraphMoneyMismatchForBudgetIncludingSubtasks],
            ["Payment(node=#1, payee=Person<'person1'>, payee_key='person1', "
             + "amount=123, state=Submitted, paid=None, "
             + "submitted=2020-05-01 00:00:00-07:23)"])
        check(
            """
            person1 = { amount = 123, paid = 2020-05-01 }
            """,
            [BudgetGraphMoneyMismatchForBudgetExcludingSubtasks,
             BudgetGraphMoneyMismatchForBudgetIncludingSubtasks],
            ["Payment(node=#1, payee=Person<'person1'>, payee_key='person1', "
             + "amount=123, state=Paid, paid=2020-05-01, "
             + "submitted=None)"])
        check(
            """
            person1 = { amount = 123, paid = 2020-05-01T00:00:00 }
            """,
            [BudgetGraphMoneyMismatchForBudgetExcludingSubtasks,
             BudgetGraphMoneyMismatchForBudgetIncludingSubtasks],
            ["Payment(node=#1, payee=Person<'person1'>, payee_key='person1', "
             + "amount=123, state=Paid, paid=2020-05-01 00:00:00, "
             + "submitted=None)"])
        check(
            """
            person1 = { amount = 123, paid = 2020-05-01T00:00:00Z }
            """,
            [BudgetGraphMoneyMismatchForBudgetExcludingSubtasks,
             BudgetGraphMoneyMismatchForBudgetIncludingSubtasks],
            ["Payment(node=#1, payee=Person<'person1'>, payee_key='person1', "
             + "amount=123, state=Paid, paid=2020-05-01 00:00:00+00:00, "
             + "submitted=None)"])
        check(
            """
            person1 = { amount = 123, paid = 2020-05-01T00:00:00-07:23 }
            """,
            [BudgetGraphMoneyMismatchForBudgetExcludingSubtasks,
             BudgetGraphMoneyMismatchForBudgetIncludingSubtasks],
            ["Payment(node=#1, payee=Person<'person1'>, payee_key='person1', "
             + "amount=123, state=Paid, paid=2020-05-01 00:00:00-07:23, "
             + "submitted=None)"])
        check(
            """
            [person1]
            amount = 123
            submitted = 2020-05-23
            paid = 2020-05-01
            """,
            [BudgetGraphMoneyMismatchForBudgetExcludingSubtasks,
             BudgetGraphMoneyMismatchForBudgetIncludingSubtasks],
            ["Payment(node=#1, payee=Person<'person1'>, payee_key='person1', "
             + "amount=123, state=Paid, paid=2020-05-01, "
             + "submitted=2020-05-23)"])
        check(
            """
            [person1]
            amount = 123
            submitted = 2020-05-23
            paid = 2020-05-01T00:00:00
            """,
            [BudgetGraphMoneyMismatchForBudgetExcludingSubtasks,
             BudgetGraphMoneyMismatchForBudgetIncludingSubtasks],
            ["Payment(node=#1, payee=Person<'person1'>, payee_key='person1', "
             + "amount=123, state=Paid, paid=2020-05-01 00:00:00, "
             + "submitted=2020-05-23)"])
        check(
            """
            [person1]
            amount = 123
            submitted = 2020-05-23
            paid = 2020-05-01T00:00:00Z
            """,
            [BudgetGraphMoneyMismatchForBudgetExcludingSubtasks,
             BudgetGraphMoneyMismatchForBudgetIncludingSubtasks],
            ["Payment(node=#1, payee=Person<'person1'>, payee_key='person1', "
             + "amount=123, state=Paid, paid=2020-05-01 00:00:00+00:00, "
             + "submitted=2020-05-23)"])
        check(
            """
            [person1]
            amount = 123
            submitted = 2020-05-23
            paid = 2020-05-01T00:00:00-07:23
            """,
            [BudgetGraphMoneyMismatchForBudgetExcludingSubtasks,
             BudgetGraphMoneyMismatchForBudgetIncludingSubtasks],
            ["Payment(node=#1, payee=Person<'person1'>, payee_key='person1', "
             + "amount=123, state=Paid, paid=2020-05-01 00:00:00-07:23, "
             + "submitted=2020-05-23)"])

    def test_payees_money_mismatch(self):
        bg = BudgetGraph([
            MockBug(bug_id=1,
                    cf_budget_parent=None,
                    cf_budget="10",
                    cf_total_budget="10",
                    cf_nlnet_milestone="milestone 1",
                    cf_payees_list="person1 = 5\nperson2 = 10",
                    summary=""),
        ], EXAMPLE_CONFIG)
        errors = bg.get_errors()
        self.assertErrorTypesMatches(errors,
                                     [BudgetGraphPayeesMoneyMismatch])
        self.assertEqual(errors[0].bug_id, 1)
        self.assertEqual(errors[0].root_bug_id, 1)
        self.assertEqual(errors[0].payees_total, 15)

    def test_payees_parse_error(self):
        def check_parse_error(cf_payees_list, expected_msg):
            errors = BudgetGraph([
                MockBug(bug_id=1,
                        cf_budget_parent=None,
                        cf_budget="0",
                        cf_total_budget="0",
                        cf_nlnet_milestone="milestone 1",
                        cf_payees_list=cf_payees_list,
                        summary=""),
            ], EXAMPLE_CONFIG).get_errors()
            self.assertErrorTypesMatches(errors,
                                         [BudgetGraphPayeesParseError])
            self.assertEqual(errors[0].bug_id, 1)
            self.assertEqual(errors[0].msg, expected_msg)

        check_parse_error("""
                          "payee 1" = []
                          """,
                          "value for key 'payee 1' is invalid -- it should "
                          "either be a monetary value or a table")

        check_parse_error("""
                          payee = "ashjkf"
                          """,
                          "failed to parse monetary amount for key 'payee': "
                          "invalid Money string: characters after sign and "
                          "before first `.` must be ascii digits")

        check_parse_error("""
                          payee = "1"
                          payee = "1"
                          """,
                          "TOML parse error: Duplicate keys! (line 3"
                          " column 1 char 39)")

        check_parse_error("""
                          payee = 123.45
                          """,
                          "failed to parse monetary amount for key 'payee': "
                          "monetary amount is not a string or integer (to "
                          "use fractional amounts such as 123.45, write "
                          "\"123.45\"): 123.45")

        check_parse_error("""
                          payee = {}
                          """,
                          "value for key 'payee' is missing the `amount` "
                          "field which is required")

        check_parse_error("""
                          payee = { amount = 123.45 }
                          """,
                          "failed to parse monetary amount for key 'payee': "
                          "monetary amount is not a string or integer (to "
                          "use fractional amounts such as 123.45, write "
                          "\"123.45\"): 123.45")

        check_parse_error("""
                          payee = { amount = 123, blah = false }
                          """,
                          "value for key 'payee' has an unknown field: `blah`")

        check_parse_error("""
                          payee = { amount = 123, submitted = false }
                          """,
                          "failed to parse `submitted` field for key "
                          "'payee': invalid date: false")

        check_parse_error("""
                          payee = { amount = 123, submitted = 123 }
                          """,
                          "failed to parse `submitted` field for key 'payee':"
                          " invalid date: 123")

        check_parse_error(
            """
            payee = { amount = 123, paid = 2020-01-01, submitted = "abc" }
            """,
            "failed to parse `submitted` field for key 'payee': "
            "invalid date: 'abc'")

        check_parse_error(
            """
            payee = { amount = 123, paid = 12:34:56 }
            """,
            "failed to parse `paid` field for key 'payee': just a time of "
            "day by itself is not enough, a date must be included: 12:34:56")

        check_parse_error(
            """
            payee = { amount = 123, submitted = 12:34:56.123456 }
            """,
            "failed to parse `submitted` field for key 'payee': just a time "
            "of day by itself is not enough, a date must be included: "
            "12:34:56.123456")

    def test_negative_payee_money(self):
        bg = BudgetGraph([
            MockBug(bug_id=1,
                    cf_budget_parent=None,
                    cf_budget="10",
                    cf_total_budget="10",
                    cf_nlnet_milestone="milestone 1",
                    cf_payees_list="""person1 = -10""",
                    summary=""),
        ], EXAMPLE_CONFIG)
        errors = bg.get_errors()
        self.assertErrorTypesMatches(errors,
                                     [BudgetGraphNegativePayeeMoney,
                                      BudgetGraphPayeesMoneyMismatch])
        self.assertEqual(errors[0].bug_id, 1)
        self.assertEqual(errors[0].root_bug_id, 1)
        self.assertEqual(errors[0].payee_key, "person1")
        self.assertEqual(errors[1].bug_id, 1)
        self.assertEqual(errors[1].root_bug_id, 1)
        self.assertEqual(errors[1].payees_total, -10)

    def test_duplicate_payments(self):
        bg = BudgetGraph([
            MockBug(bug_id=1,
                    cf_budget_parent=None,
                    cf_budget="10",
                    cf_total_budget="10",
                    cf_nlnet_milestone="milestone 1",
                    cf_payees_list="""
                    person1 = 5
                    alias1 = 5
                    """,
                    summary=""),
        ], EXAMPLE_CONFIG)
        errors = bg.get_errors()
        self.assertErrorTypesMatches(errors, [])
        person1 = EXAMPLE_CONFIG.people["person1"]
        person2 = EXAMPLE_CONFIG.people["person2"]
        person3 = EXAMPLE_CONFIG.people["person3"]
        milestone1 = EXAMPLE_CONFIG.milestones["milestone 1"]
        milestone2 = EXAMPLE_CONFIG.milestones["milestone 2"]
        node1: Node = bg.nodes[1]
        node1_payment_person1 = node1.payments["person1"]
        node1_payment_alias1 = node1.payments["alias1"]
        self.assertEqual(bg.payments, {
            person1: {
                milestone1: [node1_payment_person1, node1_payment_alias1],
                milestone2: [],
            },
            person2: {milestone1: [], milestone2: []},
            person3: {milestone1: [], milestone2: []},
        })
        self.assertEqual(
            repr(node1.payment_summaries),
            "{Person(config=..., identifier='person1', "
            "full_name='Person One', "
            "aliases=OrderedSet(['person1_alias1', 'alias1']), email=None): "
            "PaymentSummary(total=10, total_paid=0, total_submitted=0, "
            "submitted_date=None, paid_date=None, "
            "state=PaymentSummaryState.NotYetSubmitted, "
            "payments=(Payment(node=#1, payee=Person<'person1'>, "
            "payee_key='person1', amount=5, state=NotYetSubmitted, "
            "paid=None, submitted=None), Payment(node=#1, "
            "payee=Person<'person1'>, payee_key='alias1', amount=5, "
            "state=NotYetSubmitted, paid=None, submitted=None)))}")

    def test_incorrect_root_for_milestone(self):
        bg = BudgetGraph([
            MockBug(bug_id=1,
                    cf_budget_parent=None,
                    cf_budget="10",
                    cf_total_budget="10",
                    cf_nlnet_milestone="milestone 2",
                    cf_payees_list="",
                    summary=""),
        ], EXAMPLE_CONFIG)
        errors = bg.get_errors()
        self.assertErrorTypesMatches(errors,
                                     [BudgetGraphIncorrectRootForMilestone])
        self.assertEqual(errors[0].bug_id, 1)
        self.assertEqual(errors[0].root_bug_id, 1)
        self.assertEqual(errors[0].milestone, "milestone 2")
        self.assertEqual(errors[0].milestone_canonical_bug_id, 2)
        bg = BudgetGraph([
            MockBug(bug_id=1,
                    cf_budget_parent=None,
                    cf_budget="0",
                    cf_total_budget="0",
                    cf_nlnet_milestone="milestone 2",
                    cf_payees_list="",
                    summary=""),
        ], EXAMPLE_CONFIG)
        errors = bg.get_errors()
        self.assertErrorTypesMatches(errors, [])

    def test_payments(self):
        bg = BudgetGraph([
            MockBug(bug_id=1,
                    cf_budget_parent=None,
                    cf_budget="10",
                    cf_total_budget="10",
                    cf_nlnet_milestone="milestone 1",
                    cf_payees_list="person1 = 3\nperson2 = 7",
                    summary=""),
            MockBug(bug_id=2,
                    cf_budget_parent=None,
                    cf_budget="10",
                    cf_total_budget="10",
                    cf_nlnet_milestone="milestone 2",
                    cf_payees_list="person3 = 5\nperson2 = 5",
                    summary=""),
        ], EXAMPLE_CONFIG)
        self.assertErrorTypesMatches(bg.get_errors(), [])
        person1 = EXAMPLE_CONFIG.people["person1"]
        person2 = EXAMPLE_CONFIG.people["person2"]
        person3 = EXAMPLE_CONFIG.people["person3"]
        milestone1 = EXAMPLE_CONFIG.milestones["milestone 1"]
        milestone2 = EXAMPLE_CONFIG.milestones["milestone 2"]
        node1: Node = bg.nodes[1]
        node2: Node = bg.nodes[2]
        node1_payment_person1 = node1.payments["person1"]
        node1_payment_person2 = node1.payments["person2"]
        node2_payment_person2 = node2.payments["person2"]
        node2_payment_person3 = node2.payments["person3"]
        self.assertEqual(bg.payments,
                         {
                             person1: {
                                 milestone1: [node1_payment_person1],
                                 milestone2: [],
                             },
                             person2: {
                                 milestone1: [node1_payment_person2],
                                 milestone2: [node2_payment_person2],
                             },
                             person3: {
                                 milestone1: [],
                                 milestone2: [node2_payment_person3],
                             },
                         })

    def test_status(self):
        bg = BudgetGraph([MockBug(bug_id=1, status="blah")],
                         EXAMPLE_CONFIG)
        errors = bg.get_errors()
        self.assertErrorTypesMatches(errors,
                                     [BudgetGraphUnknownStatus])
        self.assertEqual(errors[0].bug_id, 1)
        self.assertEqual(errors[0].status_str, "blah")
        for status in BugStatus:
            bg = BudgetGraph([MockBug(bug_id=1, status=status)],
                             EXAMPLE_CONFIG)
            self.assertErrorTypesMatches(bg.get_errors(), [])
            self.assertEqual(bg.nodes[1].status, status)

    def test_assignee(self):
        bg = BudgetGraph([MockBug(bug_id=1, assigned_to="blah")],
                         EXAMPLE_CONFIG)
        errors = bg.get_errors()
        self.assertErrorTypesMatches(errors,
                                     [BudgetGraphUnknownAssignee])
        self.assertEqual(errors[0].bug_id, 1)
        self.assertEqual(errors[0].assignee, "blah")
        bg = BudgetGraph([MockBug(bug_id=1,
                                  assigned_to="person2@example.com")],
                         EXAMPLE_CONFIG)
        self.assertErrorTypesMatches(bg.get_errors(), [])
        self.assertEqual(bg.nodes[1].assignee,
                         EXAMPLE_CONFIG.people["person2"])

    def test_closest_bug_in_mou(self):
        bg = BudgetGraph([
            MockBug(bug_id=1, cf_nlnet_milestone="milestone 1"),
            MockBug(bug_id=2, cf_budget_parent=1,
                    cf_nlnet_milestone="milestone 1"),
            MockBug(bug_id=3, cf_budget_parent=2,
                    cf_nlnet_milestone="milestone 1"),
            MockBug(bug_id=4, cf_budget_parent=1,
                    cf_nlnet_milestone="milestone 1"),
            MockBug(bug_id=5, cf_budget_parent=4,
                    cf_nlnet_milestone="milestone 1"),
            MockBug(bug_id=6),
            MockBug(bug_id=7, cf_nlnet_milestone="bad milestone"),
            MockBug(bug_id=8, cf_budget_parent=7,
                    cf_nlnet_milestone="bad milestone"),
        ], EXAMPLE_CONFIG)
        errors = bg.get_errors()
        self.assertErrorTypesMatches(errors, [BudgetGraphUnknownMilestone,
                                              BudgetGraphUnknownMilestone])
        self.assertEqual(bg.nodes[1].closest_bug_in_mou, None)
        self.assertEqual(bg.nodes[2].closest_bug_in_mou, bg.nodes[2])
        self.assertEqual(bg.nodes[3].closest_bug_in_mou, bg.nodes[2])
        self.assertEqual(bg.nodes[4].closest_bug_in_mou, bg.nodes[4])
        self.assertEqual(bg.nodes[5].closest_bug_in_mou, bg.nodes[4])
        self.assertEqual(bg.nodes[6].closest_bug_in_mou, None)
        self.assertEqual(bg.nodes[7].closest_bug_in_mou, None)
        self.assertEqual(bg.nodes[8].closest_bug_in_mou, None)


if __name__ == "__main__":
    unittest.main()
