from collections import defaultdict
from pathlib import Path
from typing import Dict, List, Any, Optional
from io import StringIO
import enum
from budget_sync.budget_graph import BudgetGraph, Node, Payment, PayeeState, PaymentSummary
from budget_sync.config import Person, Milestone
from budget_sync.money import Money
from budget_sync.ordered_set import OrderedSet
from budget_sync.util import BugStatus


def _markdown_escape_char(char: str) -> str:
    if char == "<":
        return "&lt;"
    if char == "&":
        return "&amp;"
    if char in "\\`*_{}[]()#+-.!":
        return "\\" + char
    return char


def markdown_escape(v: Any) -> str:
    return "".join([_markdown_escape_char(char) for char in str(v)])


class DisplayStatus(enum.Enum):
    Hidden = "Hidden"
    NotYetStarted = "Not yet started"
    InProgress = "Currently working on"
    Completed = "Completed but not yet added to payees list"

    @staticmethod
    def from_status(status: BugStatus) -> "DisplayStatus":
        return _DISPLAY_STATUS_MAP[status]


_DISPLAY_STATUS_MAP = {
    BugStatus.UNCONFIRMED: DisplayStatus.Hidden,
    BugStatus.CONFIRMED: DisplayStatus.NotYetStarted,
    BugStatus.IN_PROGRESS: DisplayStatus.InProgress,
    BugStatus.DEFERRED: DisplayStatus.Hidden,
    BugStatus.RESOLVED: DisplayStatus.Completed,
    BugStatus.VERIFIED: DisplayStatus.Completed,
    BugStatus.PAYMENTPENDING: DisplayStatus.Completed,
}


class MarkdownWriter:
    last_headers: List[str]

    def __init__(self):
        self.buffer = StringIO()
        self.last_headers = []

    def write_headers(self, headers: List[str]):
        if headers == self.last_headers:
            return
        for i in range(len(headers)):
            if not headers[i].startswith("\n" + "#" * (i + 1) + " "):
                raise ValueError(
                    "invalid markdown header. if you're not trying to make a"
                    " markdown header, don't use write_headers!")
            if i >= len(self.last_headers):
                print(headers[i], file=self.buffer)
                self.last_headers.append(headers[i])
            elif headers[i] != self.last_headers[i]:
                del self.last_headers[i:]
                print(headers[i], file=self.buffer)
                self.last_headers.append(headers[i])
        if len(self.last_headers) > len(headers):
            raise ValueError("tried to go from deeper header scope stack to "
                             "ancestor scope without starting a new header, "
                             "which is not supported by markdown",
                             self.last_headers, headers)
        assert headers == self.last_headers

    def write_node_header(self,
                          headers: List[str],
                          node: Optional[Node]):
        self.write_headers(headers)
        if node is None:
            print("* None", file=self.buffer)
            return
        summary = markdown_escape(node.bug.summary)
        print(f"* [Bug #{node.bug.id}]({node.bug_url}):\n  {summary}",
              file=self.buffer)

    def write_node(self,
                   headers: List[str],
                   node: Node,
                   payment: Optional[Payment]):
        self.write_node_header(headers, node)
        if payment is not None:
            if node.fixed_budget_excluding_subtasks \
                    != node.budget_excluding_subtasks:
                total = (f"&euro;{node.fixed_budget_excluding_subtasks} ("
                         f"total is fixed from amount appearing in bug report,"
                         f" which is &euro;{node.budget_excluding_subtasks})")
            else:
                total = f"&euro;{node.fixed_budget_excluding_subtasks}"
            if payment.submitted:
                print(f"    * submitted on {payment.submitted}",
                      file=self.buffer)
            if payment.paid:
                print(f"    * paid on {payment.paid}",
                      file=self.buffer)
            if payment.amount != node.fixed_budget_excluding_subtasks \
                    or payment.amount != node.budget_excluding_subtasks:
                print(f"    * &euro;{payment.amount} out of total of {total}",
                      file=self.buffer)
            else:
                print(f"    * &euro;{payment.amount} which is the total amount",
                      file=self.buffer)
        closest = node.closest_bug_in_mou
        if closest is node:
            print(f"    * this task is a MoU Milestone",
                  file=self.buffer)
        elif closest is not None:
            print(f"    * this task is part of MoU Milestone\n"
                  f"      [Bug #{closest.bug.id}]({closest.bug_url})",
                  file=self.buffer)
        elif payment is not None:  # only report this if there's a payment
            print(f"    * neither this task nor any parent tasks are in "
                  f"the MoU",
                  file=self.buffer)


def _markdown_for_person(person: Person,
                         payments_dict: Dict[Milestone, List[Payment]],
                         assigned_nodes: List[Node],
                         nodes_subset: Optional[OrderedSet[Node]] = None,
                         ) -> str:
    def node_included(node: Node) -> bool:
        return nodes_subset is None or node in nodes_subset
    writer = MarkdownWriter()
    print(f"<!-- autogenerated by budget-sync -->", file=writer.buffer)
    writer.write_headers([f"\n# {person.full_name} ({person.identifier})\n"])
    print(file=writer.buffer)
    status_tracking_header = "\n# Status Tracking\n"
    writer.write_headers([status_tracking_header])
    displayed_nodes_dict: Dict[DisplayStatus, List[Node]]
    displayed_nodes_dict = {i: [] for i in DisplayStatus}
    for node in assigned_nodes:
        display_status = DisplayStatus.from_status(node.status)
        displayed_nodes_dict[display_status].append(node)

    def write_display_status_chunk(display_status: DisplayStatus):
        display_status_header = f"\n## {display_status.value}\n"
        for node in displayed_nodes_dict[display_status]:
            if not node_included(node):
                continue
            if display_status == DisplayStatus.Completed:
                payment_found = False
                for payment in node.payments.values():
                    if payment.payee == person:
                        payment_found = True
                        break
                if payment_found:
                    continue
                if len(node.payments) == 0 \
                        and node.budget_excluding_subtasks == 0 \
                        and node.budget_including_subtasks == 0:
                    continue
            writer.write_node(
                headers=[status_tracking_header, display_status_header],
                node=node, payment=None)

    for display_status in DisplayStatus:
        if display_status == DisplayStatus.Hidden \
                or display_status == DisplayStatus.NotYetStarted:
            continue
        write_display_status_chunk(display_status)

    for payee_state in PayeeState:
        # work out headers per status
        if payee_state == PayeeState.NotYetSubmitted:
            display_status_header = "\n## Payment not yet submitted\n"
            subtotals_msg = ("\nMoU Milestone subtotals for not "
                             "yet submitted payments\n")
        elif payee_state == PayeeState.Submitted:
            display_status_header = ("\n## Submitted to NLNet but "
                                     "not yet paid\n")
            subtotals_msg = ("\nMoU Milestone subtotals for "
                             "submitted but not yet paid payments\n")
        else:
            assert payee_state == PayeeState.Paid
            display_status_header = "\n## Paid by NLNet\n"
            subtotals_msg = ("\nMoU Milestone subtotals for paid "
                             "payments\n")
        # list all the payments grouped by Grant
        for milestone, payments_list in payments_dict.items():
            milestone_header = f"\n### {milestone.identifier}\n"
            mou_subtotals: Dict[Optional[Node], Money] = defaultdict(Money)
            headers = [status_tracking_header,
                       display_status_header,
                       milestone_header]
            # write out the payments and also compute the subtotals per
            # mou milestone
            for payment in payments_list:
                node = payment.node
                if payment.state == payee_state and node_included(node):
                    mou_subtotals[node.closest_bug_in_mou] += payment.amount
                    writer.write_node(headers=headers,
                                      node=payment.node, payment=payment)
            # now display the mou subtotals. really, this should be before
            for node, subtotal in mou_subtotals.items():
                writer.write_headers(headers)
                print(subtotals_msg, file=writer.buffer)
                writer.write_node_header(headers, node)
                if node is None:
                    budget = ""
                elif node.fixed_budget_including_subtasks \
                        != node.budget_including_subtasks:
                    budget = (" out of total including subtasks of "
                              f"&euro;{node.fixed_budget_including_subtasks}"
                              " (budget is fixed from amount appearing in "
                              "bug report, which is "
                              f"&euro;{node.budget_including_subtasks})")
                else:
                    budget = (" out of total including subtasks of "
                              f"&euro;{node.fixed_budget_including_subtasks}")
                print(f"    * subtotal &euro;{subtotal}{budget}",
                      file=writer.buffer)

    # write_display_status_chunk(DisplayStatus.NotYetStarted)

    return writer.buffer.getvalue()


def write_budget_markdown(budget_graph: BudgetGraph,
                          output_dir: Path,
                          nodes_subset: Optional[OrderedSet[Node]] = None):
    output_dir.mkdir(parents=True, exist_ok=True)
    for person, payments_dict in budget_graph.payments.items():
        markdown = _markdown_for_person(person,
                                        payments_dict,
                                        budget_graph.assigned_nodes[person],
                                        nodes_subset)
        output_file = output_dir.joinpath(person.output_markdown_file)
        output_file.write_text(markdown, encoding="utf-8")


def markdown_for_person(budget_graph: BudgetGraph, person: Person,
                        nodes_subset: Optional[OrderedSet[Node]] = None,
                        ) -> str:
    return _markdown_for_person(person, budget_graph.payments[person],
                                budget_graph.assigned_nodes[person],
                                nodes_subset)
