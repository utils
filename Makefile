.PHANTOM: build upload clean

build:
	mkdir -p task_db
	budget-sync -c budget-sync-config.toml -o task_db/mdwn

justupload:
	rsync -HPavz --no-perms --no-group --no-times -e 'ssh -p 922' --delete task_db/* \
	lkcl@libre-soc.org:/var/www/libresoc-nlnet/task_db
	ssh lkcl@libre-soc.org update-ikiwiki.sh

upload: build justupload

clean:
	rm -rf task_db
